package com.booking.test.entities;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@MappedSuperclass
@EqualsAndHashCode
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseEntity<ID extends Serializable> {

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED_AT", updatable = false)
    protected Date createdDate;

    @LastModifiedDate
    @Column(name="UPDATED_AT")
    protected Date updatedDate;

    @Enumerated(value = EnumType.ORDINAL)
    @Column(name = "RECORD_STATE", nullable = false )
    protected RecordState recordState = RecordState.ACTIVE;
}
