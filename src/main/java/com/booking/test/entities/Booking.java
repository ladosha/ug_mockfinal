package com.booking.test.entities;

import com.booking.test.dto.BookingDto;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "BOOKINGS")
public class Booking extends BaseEntity<Long> {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "bookingIdSeq", sequenceName = "BOOKING_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bookingIdSeq")
    private Long id;

    @ManyToOne
    private Room room;

    @Column(name = "START_DATE", nullable = false)
    private Date startDate;

    @Column(name = "END_DATE", nullable = false)
    private Date endDate;

    @Column(name= "CLIENT_NAME", nullable = false)
    private String clientName;

    public Booking(BookingDto bookingDto) {
        this.startDate = bookingDto.getBookingStartDate();
        this.endDate = bookingDto.getBookingEndDate();
        this.clientName = bookingDto.getClientName();
    }
}
