package com.booking.test.entities;

import com.booking.test.dto.HotelDto;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "HOTELS")
public class Hotel extends BaseEntity<Long> {

    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "hotelIdSeq", sequenceName = "HOTEL_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hotelIdSeq")
    private Long id;

    @Column(name= "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", nullable = true)
    private String description;

    public Hotel(HotelDto hotelDto) {
        this.name = hotelDto.getHotelName();
        this.description = hotelDto.getHotelDescription();
    }
}
