package com.booking.test.entities;

import com.booking.test.dto.HotelDto;
import com.booking.test.dto.RoomDto;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "ROOMS")
public class Room extends BaseEntity<Long> {
    @Id
    @Column(name="ID")
    @SequenceGenerator(name = "roomIdSeq", sequenceName = "ROOM_ID_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roomIdSeq")
    private Long id;

    @Column(name = "NUMBER", nullable = false)
    private Integer number;

    @Column(name = "AREA", nullable = false)
    private Integer area;

    @Column(name = "PRICE", nullable = false)
    private Integer price;

    @Column(name = "DESCRIPTION", nullable = true)
    private String description;

    @ManyToOne
    private Hotel hotel;

    public Room(RoomDto roomDto) {
        this.number = roomDto.getRoomNumber();
        this.area = roomDto.getRoomArea();
        this.price = roomDto.getRoomPrice();
        this.description = roomDto.getRoomDescription();
    }
}
