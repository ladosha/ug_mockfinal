package com.booking.test.entities;

public enum RecordState {
    ACTIVE, INACTIVE, DELETED
}
