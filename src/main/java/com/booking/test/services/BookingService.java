package com.booking.test.services;

import com.booking.test.apiUtils.ApiUtils;
import com.booking.test.apiUtils.IncorrectParameterException;
import com.booking.test.apiUtils.NoSuchElementFoundException;
import com.booking.test.dto.ApiResponse;
import com.booking.test.dto.BookingDto;
import com.booking.test.dto.BookingStatisticsDto;
import com.booking.test.dto.HotelDto;
import com.booking.test.entities.Booking;
import com.booking.test.entities.Hotel;
import com.booking.test.entities.Room;
import com.booking.test.repositories.BookingRepository;
import com.booking.test.repositories.HotelRepository;
import com.booking.test.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BookingService {

    private final BookingRepository bookingRepository;
    private final RoomRepository roomRepository;

    @Autowired
    public BookingService(BookingRepository bookingRepository, RoomRepository roomRepository) {
        this.bookingRepository = bookingRepository;
        this.roomRepository = roomRepository;
    }

    //DONE
    public ApiResponse addBooking(BookingDto bookingDto) {

        if(bookingDto == null ||
                bookingDto.getBookingRoomId() == null ||
                bookingDto.getBookingStartDate() == null ||
                bookingDto.getBookingEndDate() == null ||
                bookingDto.getClientName() == null) {
            throw new IncorrectParameterException().addIncorrectParameter("booking");
        }

        if(bookingDto.getBookingStartDate().after(bookingDto.getBookingEndDate())) {
            throw new IncorrectParameterException().addIncorrectParameter("booking dates");
        }

        Optional<Room> room = this.roomRepository.findById(bookingDto.getBookingRoomId());

        if(room.isEmpty()) {
            throw new NoSuchElementFoundException().addDescription("room", "by id");
        }

        Booking newBooking = new Booking(bookingDto);
        newBooking.setRoom(room.get());

        Booking booking = bookingRepository.save(newBooking);

        return ApiUtils.getApiResponse(booking);
    }

    //DONE
    public ApiResponse getAllBookings() {
        List<Booking> bookings = bookingRepository.findAll();

        return new ApiResponse().addData("bookings", bookings);
    }

    public ApiResponse getAllBookingsFromThisYear() {
        List<BookingStatisticsDto> bookingStatistics = bookingRepository.findBookingStatistics();

        return new ApiResponse().addData("bookings", bookingStatistics);
    }
}
