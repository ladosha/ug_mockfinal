package com.booking.test.services;

import com.booking.test.apiUtils.ApiUtils;
import com.booking.test.apiUtils.IncorrectParameterException;
import com.booking.test.dto.ApiResponse;
import com.booking.test.dto.HotelDto;
import com.booking.test.entities.Hotel;
import com.booking.test.repositories.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HotelService {

    private final HotelRepository hotelRepository;

    @Autowired
    public HotelService(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    //DONE
    public ApiResponse addHotel(HotelDto hotelDto) {

        if(hotelDto == null || hotelDto.getHotelName() == null ||
                hotelDto.getHotelName().isEmpty()) {
            throw new IncorrectParameterException().addIncorrectParameter("hotel");
        }

        Hotel hotel = hotelRepository.save(new Hotel(hotelDto));

        return ApiUtils.getApiResponse(hotel);
    }

    //DONE
    public ApiResponse getAllHotels() {
        List<Hotel> hotels = hotelRepository.findAll();

        return new ApiResponse().addData("hotels", hotels);
    }


}
