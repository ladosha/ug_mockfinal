package com.booking.test.services;

import com.booking.test.apiUtils.ApiUtils;
import com.booking.test.apiUtils.IncorrectParameterException;
import com.booking.test.apiUtils.NoSuchElementFoundException;
import com.booking.test.dto.ApiResponse;
import com.booking.test.dto.HotelDto;
import com.booking.test.dto.RoomDto;
import com.booking.test.entities.Hotel;
import com.booking.test.entities.Room;
import com.booking.test.repositories.HotelRepository;
import com.booking.test.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoomService {

    private final HotelRepository hotelRepository;
    private final RoomRepository roomRepository;

    @Autowired
    public RoomService(RoomRepository roomRepository, HotelRepository hotelRepository) {

        this.roomRepository = roomRepository;
        this.hotelRepository = hotelRepository;
    }

    //DONE
    public ApiResponse addRoom(RoomDto roomDto) {

        if(roomDto == null ||
                roomDto.getRoomNumber() == null ||
                roomDto.getRoomArea() == null ||
                roomDto.getRoomPrice() == null ||
                roomDto.getRoomHotelId() == null) {
            throw new IncorrectParameterException().addIncorrectParameter("room");
        }

        Optional<Hotel> hotel = this.hotelRepository.findById(roomDto.getRoomHotelId());

        if(hotel.isEmpty()) {
            throw new NoSuchElementFoundException().addDescription("hotel", "by id");
        }

        Room newRoom = new Room(roomDto);
        newRoom.setHotel(hotel.get());

        Room room = roomRepository.save(newRoom);

        return ApiUtils.getApiResponse(room);
    }

    //DONE
    public ApiResponse getAllFreeRooms() {
        List<Room> rooms = roomRepository.findAllFreeRooms();

        return new ApiResponse().addData("rooms", rooms);
    }

    public ApiResponse getAllOccupiedRooms() {
        List<Room> rooms = roomRepository.findAllOccupiedRooms();

        return new ApiResponse().addData("rooms", rooms);
    }

    public ApiResponse getAllFreeRoomsByHotelId(HotelDto hotelDto) {

        if(hotelDto == null || hotelDto.getHotelIdList() == null
        || hotelDto.getHotelIdList().size() == 0) {
            throw new IncorrectParameterException().addIncorrectParameter("hotelIdList");
        }



        List<Room> rooms = roomRepository.findAllFreeRoomsByHotelIds(hotelDto.getHotelIdList());

        return new ApiResponse().addData("rooms", rooms);
    }

}
