package com.booking.test.controllers;

import com.booking.test.dto.ApiResponse;
import com.booking.test.dto.HotelDto;
import com.booking.test.services.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class HotelController {
    private final HotelService hotelService;

    @Autowired
    public HotelController(HotelService hotelService) {

        this.hotelService = hotelService;
    }

    @PostMapping("/hotel/add")
    public ApiResponse addHotel(@RequestBody HotelDto hotelDto) {
        return hotelService.addHotel(hotelDto);
    }

    @GetMapping("/hotel/get/all")
    public ApiResponse getAllHotels() {
        return hotelService.getAllHotels();
    }
}
