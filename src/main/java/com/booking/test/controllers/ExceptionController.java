package com.booking.test.controllers;

import com.booking.test.dto.ApiResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionController {

    @ResponseBody
    @ExceptionHandler
    public ApiResponse handleUnexpectedException(Exception e) {
        try {
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.addError("unknown", e);
            return apiResponse;
        }
        catch (Exception ex) {
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.addError("unknown", ex);
            return apiResponse;
        }
    }
}