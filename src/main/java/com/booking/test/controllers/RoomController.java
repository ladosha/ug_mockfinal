package com.booking.test.controllers;

import com.booking.test.dto.ApiResponse;
import com.booking.test.dto.HotelDto;
import com.booking.test.dto.RoomDto;
import com.booking.test.services.HotelService;
import com.booking.test.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RoomController {
    private final RoomService roomService;

    @Autowired
    public RoomController(RoomService roomService) {

        this.roomService = roomService;
    }

    @PostMapping("/room/add")
    public ApiResponse addRoom(@RequestBody RoomDto roomDto) {
        return roomService.addRoom(roomDto);
    }

    @GetMapping("/room/get/free")
    public ApiResponse getAllFreeRooms() {
        return roomService.getAllFreeRooms();
    }

    @GetMapping("/room/get/occupied")
    public ApiResponse getAllOccupiedRooms() {
        return roomService.getAllOccupiedRooms();
    }

    @PostMapping("/room/get/byhotel")
    public ApiResponse getAllFreeRoomsByHotel(@RequestBody HotelDto hotelDto) {
        return roomService.getAllFreeRoomsByHotelId(hotelDto);
    }
}
