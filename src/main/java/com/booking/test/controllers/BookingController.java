package com.booking.test.controllers;

import com.booking.test.dto.ApiResponse;
import com.booking.test.dto.BookingDto;
import com.booking.test.dto.HotelDto;
import com.booking.test.entities.Booking;
import com.booking.test.services.BookingService;
import com.booking.test.services.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookingController {
    private final BookingService bookingService;

    @Autowired
    public BookingController(BookingService bookingService) {

        this.bookingService = bookingService;
    }

    @PostMapping("/booking/add")
    public ApiResponse addBooking(@RequestBody BookingDto bookingDto) {
        return bookingService.addBooking(bookingDto);
    }

    @GetMapping("/booking/get/all")
    public ApiResponse getAllBookings() {
        return bookingService.getAllBookings();
    }

    @GetMapping("/booking/get/stats")
    public ApiResponse getBookingStatistics() {
        return bookingService.getAllBookingsFromThisYear();
    }
}
