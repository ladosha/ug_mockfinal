package com.booking.test.repositories;

import com.booking.test.dto.BookingStatisticsDto;
import com.booking.test.entities.Booking;
import com.booking.test.entities.Hotel;
import com.booking.test.entities.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Long>, JpaSpecificationExecutor<Booking>  {
    @Query(value = "select h.name as hotelName, count(distinct room_id) as rentRoom, sum(price) as totalAmount from bookings b\n" +
            "inner join rooms r on b.room_id = r.id\n" +
            "inner join hotels h on r.hotel_id = h.id\n" +
            "where year(b.end_date) = year(current_date)\n" +
            "group by hotel_id", nativeQuery = true)
    List<BookingStatisticsDto> findBookingStatistics();
}
