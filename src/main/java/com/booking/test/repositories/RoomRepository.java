package com.booking.test.repositories;

import com.booking.test.entities.Hotel;
import com.booking.test.entities.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface RoomRepository extends JpaRepository<Room, Long>, JpaSpecificationExecutor<Room> {

    @Query("SELECT r FROM Room r where r.id not in " +
            "(SELECT r FROM Room r inner join Booking b on b.room.id = r.id  WHERE current_date between b.startDate and b.endDate )")
    List<Room> findAllFreeRooms();

    @Query("SELECT r FROM Room r inner join Booking b on b.room.id = r.id  WHERE current_date between b.startDate and b.endDate ")
    List<Room> findAllOccupiedRooms();


    @Query("SELECT r FROM Room r where r.hotel.id in :hotelIdList and r.id not in " +
            "(SELECT r FROM Room r inner join Booking b on b.room.id = r.id  WHERE current_date between b.startDate and b.endDate )")
    List<Room> findAllFreeRoomsByHotelIds(@Param("hotelIdList") List<Long> hotelIdList);
}
