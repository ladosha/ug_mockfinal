package com.booking.test.apiUtils;

import com.booking.test.dto.ApiResponse;

public class ApiUtils {

    public static ApiResponse getApiResponse(Object object) {

        String name  = object.getClass().getSimpleName();
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.addData(name, object);
        return apiResponse;

    }
}
