package com.booking.test.apiUtils;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class NoSuchElementFoundException extends RuntimeException {

    private Map<String, Object> description = new HashMap<>();

    public NoSuchElementFoundException() {
        super("NO_SUCH_ELEMENT_FOUND_EXCEPTION");
    }

    public NoSuchElementFoundException addDescription(String key, Object value) {
        this.description.put(key, value);
        return this;
    }
}
