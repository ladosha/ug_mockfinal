package com.booking.test.dto;

import lombok.Data;

import java.util.List;

@Data
public class HotelDto {
    private Long hotelId;
    private String hotelName;
    private String hotelDescription;
    private List<Long> hotelIdList;
}
