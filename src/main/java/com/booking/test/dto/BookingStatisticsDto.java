package com.booking.test.dto;

import lombok.Data;

public interface BookingStatisticsDto {
    String getHotelName();
    Integer getRentRoom();
    String getTotalAmount();
}
