package com.booking.test.dto;

import lombok.Data;

import java.util.Date;

@Data
public class BookingDto {
    private Long bookingId;
    private Long bookingRoomId;
    private Date bookingStartDate;
    private Date bookingEndDate;
    private String clientName;
}
