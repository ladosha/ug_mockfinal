package com.booking.test.dto;

import lombok.Data;

@Data
public class RoomDto {
    private Long roomId;
    private Integer roomNumber;
    private Integer roomArea;
    private Integer roomPrice;
    private String roomDescription;
    private Long roomHotelId;
}
