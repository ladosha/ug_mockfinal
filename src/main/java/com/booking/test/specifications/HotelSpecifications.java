package com.booking.test.specifications;

import com.booking.test.entities.Hotel;
import org.springframework.data.jpa.domain.Specification;

public class HotelSpecifications {

    private HotelSpecifications() {

    }

    public static Specification<Hotel> idEquals(Long id) {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), id));
    }

    public static Specification<Hotel> idNotNull() {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.isNotNull(root.get("id")));
    }
}
