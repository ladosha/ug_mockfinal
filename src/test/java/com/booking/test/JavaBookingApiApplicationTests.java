package com.booking.test;

import com.booking.test.dto.ApiResponse;
import com.booking.test.dto.HotelDto;
import com.booking.test.services.BookingService;
import com.booking.test.services.HotelService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class JavaBookingApiApplicationTests {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private HotelService hotelService;

    @Test
    void testBookingService() {
        ApiResponse resp = bookingService.getAllBookings();

        System.out.println(resp.getData());
    }

    @Test
    void testAddingOfHotel() {

        HotelDto dto = new HotelDto();

        dto.setHotelName("TestName");
        dto.setHotelDescription("TestDescription");

        ApiResponse resp = hotelService.addHotel(dto);

        assertEquals(0, resp.getError().size());
    }

}
